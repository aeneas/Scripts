#!/bin/bash
##########################################################
# Version: 0.1
# Date   : 26/11/2022
# Author : Aeneas
# Url    : https://codeberg.org/aeneas/
#
# Description:
# --------------------------------------------------------
# Download the latest version of Seamonkey
# inside the $HOME/.local/share/ directory
# creates a simlink named seamonkey-latest to
# $HOME/.local/bin/ and a seamonkey-latest.desktop
# file into $HOME/.local/share/applications/
# --------------------------------------------------------
##########################################################

LANG=it
DESKTOP_FILE_PATH=$HOME/.local/share/applications/
BIN_PATH=$HOME/.local/bin/
VERSION=2.53.14  # To be changed
########### The .desktop file ############################

DESKTOP_FILE="
[Desktop Entry]
Version=1.0
Name=Seamonkey Latest
GenericName=Web Browser
Comment=Browse the Web
Exec=$HOME/.local/bin/seamonkey-latest --name seamonkey-latest %u
Icon=$HOME/.local/share/seamonkey/chrome/icons/default/default128.png
Terminal=false
Type=Application
MimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
StartupWMClass=Seamonkey
Categories=Network;WebBrowser;
Keywords=web;browser;internet;
Actions=new-window;new-private-window;profile-manager;

[Desktop Action new-window]
Name=New Window
Exec=$HOME/.local/bin/seamonkey-latest --name seamonkey-latest %u --new-window %u

[Desktop Action new-private-window]
Name=New Private Window
Exec=$HOME/.local/bin/seamonkey-latest --name seamonkey-latest %u --private %u
"
##########################################################

echo -en "\nDownloading latest Seamonkey version to ~/.local/share/\n"
URL="https://archive.mozilla.org/pub/seamonkey/releases/$VERSION/linux-x86_64/$LANG/seamonkey-$VERSION.$LANG.linux-x86_64.tar.bz2"

wget -qO- $URL | 
tar -vxj -C $HOME/.local/share/ 2> /dev/null

echo -en "\nDownload Completed\n"

echo -en "\nCreating simlink to ~/.local/bin/seamonkey-latest \n"

if [ ! -d "$BIN_PATH" ];
then
	echo -n "Directory $BIN_PATH does not exists. Creating it."
	mkdir $HOME/.local/bin/
fi

echo -en "\nCreating seamonkey-latest.desktop file into $DESKTOP_FILE_PATH\n"
printf "%s" "$DESKTOP_FILE" > "$DESKTOP_FILE_PATH/seamonkey-latest.desktop"


echo -en "\n> DONE. Enjoy the latest version of Seamonkey Browser\n"

