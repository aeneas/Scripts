#!/bin/bash
##########################################################
# Version: 0.1a
# Date   : 08 Sept 2022
# Author : Aeneas
# Url    : https://codeberg.org/aeneas/
#
# Description:
# --------------------------------------------------------
# Download the latest version of Firefox
# inside the $HOME/.local/share/ directory
# creates a simlink named firefox-latest to
# $HOME/.local/bin/ and a firefox-latest.desktop
# file into $HOME/.local/share/applications/
# --------------------------------------------------------
##########################################################

LANG=it
DESKTOP_FILE_PATH=$HOME/.local/share/applications/
BIN_PATH=$HOME/.local/bin/

########### The .desktop file ############################

DESKTOP_FILE="
[Desktop Entry]
Version=1.0
Name=Firefox Latest
GenericName=Web Browser
Comment=Browse the Web
Exec=$HOME/.local/bin/firefox-latest --name firefox-latest %u
Icon=$HOME/.local/share/firefox/browser/chrome/icons/default/default128.png
Terminal=false
Type=Application
MimeType=text/html;text/xml;application/xhtml+xml;application/vnd.mozilla.xul+xml;text/mml;x-scheme-handler/http;x-scheme-handler/https;
StartupNotify=true
StartupWMClass=Firefox
Categories=Network;WebBrowser;
Keywords=web;browser;internet;
Actions=new-window;new-private-window;profile-manager;

[Desktop Action new-window]
Name=New Window
Exec=$HOME/.local/bin/firefox-latest --name firefox-latest %u --new-window %u

[Desktop Action new-private-window]
Name=New Private Window
Exec=$HOME/.local/bin/firefox-latest --name firefox-latest %u --private-window %u
"
##########################################################

echo -en "\nDownloading latest Firefox version to ~/.local/share/\n"

wget -qO- "https://download.mozilla.org/?product=firefox-latest-ssl&os=linux64&lang=$LANG" | 
tar -vxj -C $HOME/.local/share/ 2> /dev/null

echo -en "\nDownload Completed\n"

echo -en "\nCreating simlink to ~/.local/bin/firefox-latest \n"
if [ ! -d "$BIN_PATH" ];
then
	echo -n "Directory $BIN_PATH does not exists. Creating it."
	mkdir $HOME/.local/bin/
fi

ln -s $HOME/.local/share/firefox/firefox $HOME/.local/bin/firefox-latest
echo -en "\nCreating firefox-latest.desktop file into $DESKTOP_FILE_PATH\n"
printf "%s" "$DESKTOP_FILE" > "$DESKTOP_FILE_PATH/firefox-latest.desktop"


echo -en "\n> DONE. Enjoy the latest version of Mozilla Firefox\n"

